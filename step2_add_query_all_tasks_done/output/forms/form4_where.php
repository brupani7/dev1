<div class="panel">          
            <div class="panel-heading" role="tab">
                <?php
                if(isset($queryPrefillData) && !empty($queryPrefillData['field_where_columns'])) {
                ?>
                <a class="panel-title" data-parent="#accordion_build_query" data-toggle="collapse" href="#tab_add_conditions" aria-controls="tab_add_conditions" aria-expanded="true">
                  Add Conditions
                </a>
            </div>
            <div class="panel-collapse collapse in" id="tab_add_conditions" role="tabpanel" aria-expanded="true">
                <?php
                }else{
                ?>
                <a class="panel-title collapsed" data-parent="#accordion_build_query" data-toggle="collapse" href="#tab_add_conditions" aria-controls="tab_add_conditions" aria-expanded="false">
                    Add Conditions
                </a>
            </div>
            <div class="panel-collapse collapse" id="tab_add_conditions" role="tabpanel">
                <?php
                }
                ?>

                    <div class="panel-body duplicate_append_to">
                              <div class="row ">
                                        <div class="col-md-12 duplicate_append_to_inner ">
	                                        <?php
	                                        if(isset($queryPrefillData) && !empty($queryPrefillData['field_where_columns'])){
		                                        for ( $i = 0; $i < count( $queryPrefillData['field_where_columns'] ); $i ++ ) {
			                                        ?>
                                                    <div class="row table_row">
                                                        <div class="form-group col-3">
                                                            <select name="field_where_columns[]" class="form-control fill_with_all_table_columns">
                                                                <option value="" selected disabled>WHERE Column</option>
	                                                            <?php
	                                                            foreach ($database_table_columns as $database_table_column){?>
                                                                    <option <?php if(isset($queryPrefillData['field_where_columns'][$i]) && $queryPrefillData['field_where_columns'][$i]== $database_table_column){ echo "selected"; } ?> value="<?php echo $database_table_column; ?>"><?php echo $database_table_column; ?></option>
	                                                            <?php }
	                                                            ?>
                                                            </select>
                                                            <a href="#" class="table_row_delete">Delete Column</a>
                                                        </div>
                                                        <div class="form-group col-3">
                                                            <select name="field_where_columns_conditions[]"
                                                                    class="form-control">
                                                                <option value="" selected disabled>CONDITION</option>
                                                                <option  <?php if(isset($queryPrefillData['field_where_columns_conditions'][$i]) && $queryPrefillData['field_where_columns_conditions'][$i]== "="){ echo "selected"; } ?> value="="> =</option>
                                                                <option  <?php if(isset($queryPrefillData['field_where_columns_conditions'][$i]) && $queryPrefillData['field_where_columns_conditions'][$i]== "!="){ echo "selected"; } ?> value="!="> !=</option>
                                                                <option  <?php if(isset($queryPrefillData['field_where_columns_conditions'][$i]) && $queryPrefillData['field_where_columns_conditions'][$i]== "<"){ echo "selected"; } ?> value="<"> <</option>
                                                                <option  <?php if(isset($queryPrefillData['field_where_columns_conditions'][$i]) && $queryPrefillData['field_where_columns_conditions'][$i]== ">"){ echo "selected"; } ?> value=">"> ></option>
                                                                <option  <?php if(isset($queryPrefillData['field_where_columns_conditions'][$i]) && $queryPrefillData['field_where_columns_conditions'][$i]== ">="){ echo "selected"; } ?> value=">="> >=</option>
                                                                <option  <?php if(isset($queryPrefillData['field_where_columns_conditions'][$i]) && $queryPrefillData['field_where_columns_conditions'][$i]== "<="){ echo "selected"; } ?> value="<="> <=</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-3">
                                                            <select name="field_where_columns_compare_columns[]" class="form-control fill_with_all_table_columns">
                                                                <option value="" selected disabled>Compare Column</option>
	                                                            <?php
	                                                            foreach ($database_table_columns as $database_table_column){?>
                                                                    <option <?php if(isset($queryPrefillData['field_where_columns_compare_columns'][$i]) && $queryPrefillData['field_where_columns_compare_columns'][$i]== $database_table_column){ echo "selected"; } ?> value="<?php echo $database_table_column; ?>"><?php echo $database_table_column; ?></option>
	                                                            <?php }
	                                                            ?>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-3">
                                                            <input name="field_where_columns_compare_values[]" type="text" class="form-control"
                                                                placeholder="OR Compare Value" value="<?php if(isset($queryPrefillData['field_where_columns_compare_values'][$i])){ echo $queryPrefillData['field_where_columns_compare_values'][$i]; } ?>">
                                                        </div>
                                                    </div>
			                                        <?php
		                                        }
	                                        }
	                                        else{
		                                        ?>
                                                <div class="row table_row">
                                                    <div class="form-group col-3">
                                                        <select name="field_where_columns[]"
                                                                class="form-control fill_with_all_table_columns">
                                                            <option value="" selected disabled>WHERE Column</option>
                                                        </select>
                                                        <a href="#" class="table_row_delete">Delete Column</a>
                                                    </div>
                                                    <div class="form-group col-3">
                                                        <select name="field_where_columns_conditions[]"
                                                                class="form-control">
                                                            <option value="" selected disabled>CONDITION</option>
                                                            <option value="="> =</option>
                                                            <option value="!="> !=</option>
                                                            <option value="<"> <</option>
                                                            <option value=">"> ></option>
                                                            <option value=">="> >=</option>
                                                            <option value="<="> <=</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-3">
                                                        <select name="field_where_columns_compare_columns[]"
                                                                class="form-control fill_with_all_table_columns">
                                                            <option value="" selected disabled>Compare Column
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-3">
                                                        <input name="field_where_columns_compare_values[]"
                                                               type="text" class="form-control"
                                                               placeholder="OR Compare Value" value="">
                                                    </div>
                                                </div>
		                                        <?php
	                                        }
	                                        ?>
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col-12">
                                                  <button type="button" class="duplicate_row btn btn-primary waves-effect waves-classic">Add Condition</button>
                                        </div>
                              </div>
                    </div>
          </div>
</div>
