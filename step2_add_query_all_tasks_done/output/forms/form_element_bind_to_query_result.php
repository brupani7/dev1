<div class="row duplicate_append_to">
	<div id="table_row_fields" class="col-12 duplicate_append_to_inner">
		<div class="row row_master table_row">
			<div class="form-group col-4">
				<select name="field_element_db_query_use[]" class="form-control">
					<option value="select">Query Use Type</option>
					<option value="select">Output Value</option>
					<option value="select">Wrap in While Loop</option>
				</select>
				<a href="#" class="table_row_delete">Delete Row</a>
			</div>
			<div class="form-group col-4">
				<select name="field_element_db_bind_to_field[]" class="form-control">
					<option value="select">Bind to Element Field</option>
				</select>
			</div>
			<div class="form-group col-4">
				<select name="db_element_db_result_field_name[]" class="form-control table_name">
					<option value="select">Result Field Name</option>
				</select>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-12">
		<button type="button" class="duplicate_row btn btn-primary waves-effect waves-classic">Add Another</button>
	</div>
</div>

<script type="text/javascript">

var url = '/incoming-data.php';

jQuery(document).on('click', '.duplicate_row', function(event) {
	event.preventDefault();

	var parent_container = jQuery(this).closest('.duplicate_append_to');
	var total_table_rows = jQuery(parent_container).find('.table_row').length;
	var target           = jQuery(parent_container).find('.table_row').last();
	var new_id           = total_table_rows + 1;
	var place_here       = jQuery(this).closest('.duplicate_append_to').find('.duplicate_append_to_inner');
	var clone            = jQuery(target).clone().attr('id', new_id).appendTo(place_here);
});
jQuery(document).on('click', '.table_row_delete', function(event) {
	event.preventDefault();
	var total = jQuery(this).closest('.duplicate_append_to').find('.table_row').length;
	if (total > 1) {
		jQuery(this).closest('.table_row').remove();
	}
});
</script>
