<div class="panel">
	<div class="panel-heading" role="tab">
		<?php
		if(isset($queryPrefillData) && !empty($queryPrefillData['db_table_name1'])) {
		?>
		<a class="panel-title" data-parent="#accordion_build_query" data-toggle="collapse"
		   href="#tab_add_tables_insert" aria-controls="tab_add_tables" aria-expanded="true">
			Add Tables
		</a>
	</div>
	<div class="panel-collapse collapse in" id="tab_add_tables_insert" role="tabpanel" aria-expanded="true">
		<?php
		}else{
		?>
		<a class="panel-title collapsed" data-parent="#accordion_build_query" data-toggle="collapse"
		   href="#tab_add_tables_insert" aria-controls="tab_add_tables_insert" aria-expanded="false">
			Add Tables
		</a>
	</div>
	<div class="panel-collapse collapse" id="tab_add_tables_insert" role="tabpanel" style="">
		<?php
		}
		?>
		<div class="panel-body duplicate_append_to">
			<div class="row">
				<div id="table_row_fields" class="col-md-12 duplicate_append_to_inner">
					<div class="row table_row">
						<div class="col-6 mainDiv">
							<div class="form-group">
								<select name="table_name" class="form-control table_name" onchange="tableColumnsInsert(this.value,this)">
									<option value="" selected disabled>Table</option>
									<?php
									if(isset($queryPrefillData) && isset($queryPrefillData['db_table_name1']) && !empty($queryPrefillData['field_database_name'])) {
										$field_database_name=$queryPrefillData['field_database_name'];
										$result2         = mysqli_query( $connection1, "SHOW TABLES FROM `$field_database_name`" );
										$database_tables = array();
										while ( $row2 = mysqli_fetch_array( $result2 ) ) {
											$database_tables[] = $row2[0];
										}
									}
									?>
									<?php foreach ($database_tables as $value): ?>
										<option <?php if(isset($queryPrefillData) && isset($queryPrefillData['db_table_name1']) && $queryPrefillData['db_table_name1'] == $value ){ echo "selected"; } ?> value="<?php echo $value ?>"><?php echo $value ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col-md-12 tableInsertColumnsDiv"></div>
						</div>

					</div>
				</div>
			</div>

			<!--			<div class="row">-->
			<!--				<div class="col-12">-->
			<!--					<hr>-->
			<!--					<button type="button" class="duplicate_row btn btn-primary waves-effect waves-classic float-right">Add Another Table</button>-->
			<!--				</div>-->
			<!--			</div>-->

		</div>
	</div>
</div>
