<?php
//===============If isset set query id in the session, then fetch records for that query_id====================
if(isset($_SESSION['query_id'])){
	$qId=$_SESSION['query_id'];
	$queryDetails  = mysqli_query($connection,"SELECT * FROM `builder6_canvas_query` where query_id=$qId");
	if(mysqli_num_rows($queryDetails) > 0){
		$queryData = mysqli_fetch_assoc($queryDetails);
		$queryPrefillData=json_decode($queryData['query_parts_prefill'],true)[0];
	}
	else{
		unset($_SESSION['query_id']);
	}
}

//=============================================================================================================
$result1 = mysqli_query($connection1,"SHOW DATABASES");
$databases = array();
while ($row1 = mysqli_fetch_array($result1)) {
	$databases[] = $row1[0];
}
$array_queries 	  = array();

//this code was not fetching all the queries from database, So replaced by below code
//$query_load_all_queries    = "SELECT `query_id`,`query_name` FROM `builder6_canvas_query`";
//if ($stmt_load_all_queries = $connection->prepare($query_load_all_queries)) {
//
//	if(!$stmt_load_all_queries->execute()){
//		echo $stmt_load_all_queries->error;
//		$stmt_load_all_queries->free_result();
//		$stmt_load_all_queries->close();
//		exit();
//	}else {
//		$stmt_load_all_queries->store_result();
//		$total_result = $stmt_load_all_queries->num_rows;
//		if ($total_result > 0 ) {
//			$stmt_load_all_queries->bind_result($query_id,$query_name);
//			$stmt_load_all_queries->fetch();
//
//			$array_queries[] = array(
//				'query_id'   => $query_id,
//				'query_name' => $query_name
//			);
//		}
//	}
//}
//else {
//	echo 'prepare failed 1';
//}
$query_load_all_queries    = "SELECT `query_id`,`query_name` FROM `builder6_canvas_query`";
$load_all_queries_result=mysqli_query($connection,$query_load_all_queries);
if(mysqli_num_rows($load_all_queries_result)>0){
	while($all_queries=mysqli_fetch_assoc($load_all_queries_result)){
		$array_queries[]=['query_id'=>$all_queries['query_id'],'query_name'=>$all_queries['query_name']];
	}
}
$array_site_pages = array();
$query_site_pages = "SELECT `page_id`, `page_name`, `page_slug` FROM `builder2_section_pages`";

if ($stmt_load_site_pages = $connection->prepare($query_site_pages)) {
	//$stmt_load_site_pages-> bind_param('i', $_SESSION['project_id']);

	if(!$stmt_load_site_pages->execute()){
		echo $stmt_load_site_pages->error;
		$stmt_load_site_pages->free_result();
		$stmt_load_site_pages->close();
		exit();
	}else {
		$stmt_load_site_pages->store_result();
		$total_result_pages = $stmt_load_site_pages->num_rows;
		if ($total_result_pages > 0 ) {
			$stmt_load_site_pages->bind_result($page_id,$page_name,$page_slug);
			while ($stmt_load_site_pages->fetch()) {
				$array_site_pages[] = array(
					'page_id' => $page_id,
					'page_name' => $page_name,
					'page_slug' => $page_slug
				);
			}
		}else {
			echo 'no results fetch pages';
		}
	}
}else {
	echo 'prepare failed 1';
}
?>
<div class="container">
    <div class="panel panel-bordered min_height_800">
        <div class="panel-body">


            <div id="query_id_<?php echo $_SESSION['query_id'] ?>" class="row mb-20">
                <div class="col-8">
                    <div class="row">
                        <h3>Query ID: <?php echo $_SESSION['query_id']; ?></h3>
                        <form id="form_new_query" name="form_new_query" class="col-12" action="#" method="post">
                            <div class="row">
                                <div class="form-group col-3">
                                    <select id="field_load_query_file" name="field_load_query_file" class="form-control" onchange="loadQuery(this.value)">
                                        <option value="">Load Query</option>
										<?php foreach ($array_queries as $key): ?>
                                            <option <?php if(isset($_SESSION['query_id']) && $_SESSION['query_id'] == $key['query_id'] ){ echo "selected"; } ?> value="<?php echo $key['query_id'] ?>"><?php echo $key['query_id'] ?></option>
										<?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-3">
                                    <select id="field_database_name" name="field_database_name" class="form-control">
                                        <option value="select">Database Name</option>
										<?php foreach ($databases as $value): ?>
                                            <option <?php if(isset($queryPrefillData) && isset($queryPrefillData['field_database_name']) && $queryPrefillData['field_database_name'] == $value ){ echo "selected"; } ?> value="<?php echo $value ?>"><?php echo $value ?></option>
										<?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="form-group col-3">
                                    <input type="text" id="field_new_query_file_name" name="field_new_query_file_name" class="form-control" placeholder="New Query Name" value="<?php if(isset($queryData) && $queryData['query_name'] != "" ){ echo $queryData['query_name']; } ?>">
                                </div>
                                <div class="form-group col-3">
                                    <select id="field_query_file_type" name="field_query_file_type" class="form-control" onchange="queryTypeChanged(this.value)">
                                        <option selected disabled value="">Select Query Type</option>
                                        <option  <?php if(isset($queryPrefillData) && isset($queryPrefillData['field_query_file_type']) && $queryPrefillData['field_query_file_type'] == "Single Select"){ echo "selected"; } ?> value="Single Select">Single Select</option>
                                        <option  <?php if(isset($queryPrefillData) && isset($queryPrefillData['field_query_file_type']) && $queryPrefillData['field_query_file_type'] == "Join Select"){ echo "selected"; } ?> value="Join Select">Join Select</option>
                                        <option  <?php if(isset($queryPrefillData) && isset($queryPrefillData['field_query_file_type']) && $queryPrefillData['field_query_file_type'] == "Update"){ echo "selected"; } ?> value="Update">Update</option>
                                        <option  <?php if(isset($queryPrefillData) && isset($queryPrefillData['field_query_file_type']) && $queryPrefillData['field_query_file_type'] == "Insert"){ echo "selected"; } ?> value="Insert">Insert</option>
                                        <option  <?php if(isset($queryPrefillData) && isset($queryPrefillData['field_query_file_type']) && $queryPrefillData['field_query_file_type'] == "Delete"){ echo "selected"; } ?> value="Delete">Delete</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-4">
                    <div class="btn-group  float-right" role="group">
                        <button id="btn_form_query_builder_clear" type="button" class="btn btn-primary waves-effect waves-classic">
                            Clear All
                        </button>
                        <button id="btn_form_query_builder_submit"  type="button" class="btn btn-primary waves-effect waves-classic">
                            Export Query
                        </button>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-7">
                    <div class="col-12">

                    </div>
                    <div id="accordion_build_query" class="panel-group panel-group-continuous selectQueryPart hidden" aria-multiselectable="true" role="tablist">
                        <form id="form_query_buider" name="form_query_buider" action="#" method="post"  data-query_id="<?php echo $_SESSION['query_id'] ?>" >
			                <?php
			                require_once(__DIR__ .  '/form2_add_tables.php');
			                require_once(__DIR__ .  '/form3_add_columns.php');
			                require_once(__DIR__ .  '/form4_where.php');
			                require_once(__DIR__ .  '/form5_group_by.php');
			                require_once(__DIR__ .  '/form6_order_by.php');
			                require_once(__DIR__ .  '/form7_limit.php');
			                require_once(__DIR__ .  '/form8_bind_to_pages.php');
			                ?>
                        </form>
                    </div>
                    <div id="accordion_build_insert_query" class="panel-group panel-group-continuous insertQueryPart hidden" aria-multiselectable="true" role="tablist">
                        <form id="form_insert_query_buider" name="form_insert_query_buider" action="#" method="post"  data-query_id="<?php echo $_SESSION['query_id'] ?>" >
			                <?php
			                require_once(__DIR__ .  '/form2_insert_add_tables.php');
			                ?>
                        </form>
                    </div>
                    <div class="col-12">

                    </div>
                </div>
                <div class="col-5 mh-300">
					<?php require_once(get_template_directory() . '/4_builder/builder3-section-editor/step1_add_element/output/editor/editor7_canvas_file_query_preview.php'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var array   = [];
    function tableColumns1(tblObject,tblName){
        var field_database_name 	= jQuery('#field_database_name').val();
        var url 			= '/incoming-data.php';
//        var url 			= '../../actions/action3_list_database_table_columns.php';
        var table_name_as 		= jQuery('.table_name_as1').val();
        var data1   = new FormData();
        data1.append("activate_get_database_table_columns", true);
        data1.append("field_database_name", field_database_name);
        data1.append("tableName", tblName);
        data1.append("table_name_as", table_name_as);
        jQuery.ajax({
            cache: false,
            async: true,
            type: "POST",
            url: url,
            data: data1,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
                console.log(data);
                jQuery.each(data, function(index,column) {
                    if(table_name_as != "" && table_name_as != undefined){
                        if(array.indexOf(table_name_as+'.'+column) === -1) {
                            array.push(table_name_as+'.'+column);
                        }
                        if(array.indexOf(tblName+'.'+column) != -1) {
                            array.splice( array.indexOf(tblName+'.'+column), 1 )
                        }
                    }
                    else{
                        array.push(tblName+'.'+column);
                    }

                });
                var html1="<option value='' selected disabled>Select</option>";
                jQuery.each(data, function(index,column) {
                    if(table_name_as != "" && table_name_as != undefined) {
                        html1 += "<option value='" + table_name_as +'.'+column + "'>" +  table_name_as +'.'+column + "</option>";
                    }
                    else{
                        html1 += "<option value='" + tblName+'.'+column + "'>" +  tblName +'.'+column + "</option>";
                    }
                });
                var html="<option value='' selected disabled>Select</option>";
                jQuery.each(array, function(index,array_column) {
                    if(table_name_as != "" && table_name_as != undefined) {
                        html += "<option value='" + array_column + "'>" +  array_column + "</option>";
                    }
                    else{
                        html += "<option value='" +array_column + "'>" +array_column + "</option>";
                    }
                });
                jQuery('.fill_with_all_table_columns').html(html);
                jQuery('.fill_with_all_table_columns1').html(html);
            },
            error:function(e){
                console.log(e);
            }
        });
    }
    function tableColumns2(object,tblName){
        var field_database_name = jQuery('#field_database_name').val();
        var url 			= '/incoming-data.php';
//        var url 			= '../../actions/action3_list_database_table_columns.php';
        var table_name_as = jQuery(this).closest('.table_name_as2').val();
        var data1   = new FormData();
        data1.append("activate_get_database_table_columns", true);
        data1.append("field_database_name", field_database_name);
        data1.append("tableName", tblName);
        data1.append("table_name_as", table_name_as);

        jQuery.ajax({
            cache: false,
            async: true,
            type: "POST",
            url: url,
            data: data1,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
                jQuery.each(data, function(index,column) {
                    if(table_name_as != "" && table_name_as != undefined){
                        if(array.indexOf(table_name_as+'.'+column) === -1) {
                            array.push(table_name_as+'.'+column);
                        }
                        if(array.indexOf(tblName+'.'+column) != -1) {
                            array.splice( array.indexOf(tblName+'.'+column), 1 )
                        }
                    }
                    else{
                        array.push(tblName+'.'+column);
                    }

                });
                var html1="<option value='' selected disabled>Select</option>";
                jQuery.each(data, function(index,column) {
                    if(table_name_as != "" && table_name_as != undefined) {
                        html1 += "<option value='" + table_name_as +'.'+column + "'>" +  table_name_as +'.'+column + "</option>";
                    }
                    else{
                        html1 += "<option value='" + tblName+'.'+column + "'>" +  tblName +'.'+column + "</option>";
                    }
                });
                var html="<option value='' selected disabled>Select</option>";
                jQuery.each(array, function(index,array_column) {
                    if(table_name_as != "" && table_name_as != undefined) {
                        html += "<option value='" + array_column + "'>" +  array_column + "</option>";
                    }
                    else{
                        html += "<option value='" +array_column + "'>" +array_column + "</option>";
                    }
                });
                jQuery('.fill_with_all_table_columns').html(html);
                jQuery(object).closest('.row_master').find('.fill_with_all_table_columns2').html(html1);
                jQuery('.fill_with_all_table_columns1').html(html);
            }
        });
    }
    function tableColumnsAs1(object,table_name_as){
        var field_database_name = jQuery('#field_database_name').val();
        var url 			= '/incoming-data.php';
//        var url 			= '../../actions/action3_list_database_table_columns.php';
        var tblName = jQuery('.table_name1').val();
        var data   = new FormData();
        data.append("activate_get_database_table_columns", true);
        data.append("field_database_name", field_database_name);
        data.append("tableName", tblName);
        data.append("table_name_as", table_name_as);
        jQuery.ajax({
            cache: false,
            async: true,
            type: "POST",
            url: url,
            data: data,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
                jQuery.each(data, function(index,column) {
                    if(table_name_as != "" && table_name_as != undefined){
                        if(array.indexOf(table_name_as+'.'+column) === -1) {
                            array.push(table_name_as+'.'+column);
                        }
                        if(array.indexOf(tblName+'.'+column) != -1) {
                            array.splice( array.indexOf(tblName+'.'+column), 1 )
                        }
                    }
                    else{
                        array.push(tblName+'.'+column);
                    }

                });
                var html1="<option value='' selected disabled>Select</option>";
                jQuery.each(data, function(index,column) {
                    if(table_name_as != "" && table_name_as != undefined) {
                        html1 += "<option value='" + table_name_as +'.'+column + "'>" +  table_name_as +'.'+column + "</option>";
                    }
                    else{
                        html1 += "<option value='" + tblName+'.'+column + "'>" +  tblName +'.'+column + "</option>";
                    }
                });
                var html="<option value='' selected disabled>Select</option>";
                jQuery.each(array, function(index,array_column) {
                    if(table_name_as != "" && table_name_as != undefined) {
                        html += "<option value='" + array_column + "'>" +  array_column + "</option>";
                    }
                    else{
                        html += "<option value='" +array_column + "'>" +array_column + "</option>";
                    }
                });
                jQuery('.fill_with_all_table_columns').html(html);
            }
        });
    }
    function tableColumnsAs2(object,table_name_as){
        var field_database_name = jQuery('#field_database_name').val();
        var url 			= '/incoming-data.php';
//        var url 			= '../../actions/action3_list_database_table_columns.php';
        var tblName = jQuery(object).closest('.row_master').find('.table_name2').val();
        var data   = new FormData();
        data.append("activate_get_database_table_columns", true);
        data.append("field_database_name", field_database_name);
        data.append("tableName", tblName);
        data.append("table_name_as", table_name_as);
        jQuery.ajax({
            cache: false,
            async: true,
            type: "POST",
            url: url,
            data: data,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
                jQuery.each(data, function(index,column) {
                    if(table_name_as != "" && table_name_as != undefined){
                        if(array.indexOf(table_name_as+'.'+column) === -1) {
                            array.push(table_name_as+'.'+column);
                        }
                        if(array.indexOf(tblName+'.'+column) != -1) {
                            array.splice( array.indexOf(tblName+'.'+column), 1 )
                        }
                    }
                    else{
                        array.push(tblName+'.'+column);
                    }

                });
                var html1="<option value='' selected disabled>Select</option>";
                jQuery.each(data, function(index,column) {
                    if(table_name_as != "" && table_name_as != undefined) {
                        html1 += "<option value='" + table_name_as +'.'+column + "'>" +  table_name_as +'.'+column + "</option>";
                    }
                    else{
                        html1 += "<option value='" + tblName+'.'+column + "'>" +  tblName +'.'+column + "</option>";
                    }
                });
                var html="<option value='' selected disabled>Select</option>";
                jQuery.each(array, function(index,array_column) {
                    if(table_name_as != "" && table_name_as != undefined) {
                        html += "<option value='" + array_column + "'>" +  array_column + "</option>";
                    }
                    else{
                        html += "<option value='" +array_column + "'>" +array_column + "</option>";
                    }
                });

                jQuery('.fill_with_all_table_columns').html(html);
                jQuery(object).closest('.row_master').find('.fill_with_all_table_columns2').html(html1);
                jQuery('.fill_with_all_table_columns1').html(html);
            }
        });
    }
    jQuery(document).ready(function($) {
        jQuery(document).on('change', '#field_database_name', function(event) {
            var field_database_name = jQuery(this).val();
            var url 			= '/incoming-data.php';
//            var url 			= '../../actions/action2_list_database_tables.php';
            var data   = new FormData();
            data.append("activate_get_database_tables", true);
            data.append("field_database_name", field_database_name);
            jQuery.ajax({
                cache: false,
                async: true,
                type: "POST",
                url: url,
                data: data,
                contentType: false,
                processData: false,
                success: function (data) {
                    jQuery('.table_name').html(data);
                },
                error: function (e) {
                    alert(e);
                }
            });
        });

        jQuery(document).on('click', '.duplicate_row', function(event) {
            event.preventDefault();

            var parent_container = jQuery(this).closest('.duplicate_append_to');
            var total_table_rows = jQuery(parent_container).find('.table_row').length;
            var target           = jQuery(parent_container).find('.table_row').last();
            var new_id           = total_table_rows + 1;
            var place_here       = jQuery(this).closest('.duplicate_append_to').find('.duplicate_append_to_inner');
            var clone            = jQuery(target).clone().attr('id', new_id).appendTo(place_here);
        });
        jQuery(document).on('click', '.table_row_delete', function(event) {
            event.preventDefault();
            var total = jQuery(this).closest('.duplicate_append_to').find('.table_row').length;
            if (total > 1) {
                jQuery(this).closest('.table_row').remove();
            }
        });
        jQuery(document).on('click', '.move_left option:selected', function(event) {
            event.preventDefault();
            var selection   = jQuery(this);
            var target      = jQuery(this).closest('.row').find('.move_right');

            jQuery(selection).clone().appendTo(target).prop("selected", true);
            jQuery(this).remove();
        });
        jQuery(document).on('click', '.move_right option:selected', function(event) {
            event.preventDefault();
            var selection   = jQuery(this);
            var target      = jQuery(this).closest('.row').find('.move_left');

            jQuery(selection).clone().appendTo(target);
            jQuery(this).remove();
        });
        jQuery(document).on('click', '#btn_form_query_builder_submit', function(event) {

            var url 	= '/incoming-data.php';
//            var url 	= '../../actions/action4_build_query.php';

            var queryType=jQuery('#field_query_file_type').val();
            if(queryType == "Single Select" || queryType == "Join Select"){
                var data   = new FormData(jQuery("#form_query_buider")[0]);
                data.append("activate_builder_query", true);
            }
            else if(queryType == "Insert"){
                var data   = new FormData(jQuery("#form_insert_query_buider")[0]);
                data.append("activate_builder_insert_query", true);
            }
            else if(queryType == "Update"){
                var data   = new FormData(jQuery("#form_update_query_buider")[0]);
                data.append("activate_update_builder_query", true);
            }
            else if(queryType == "Delete"){
                var data   = new FormData(jQuery("#form_delete_query_buider")[0]);
                data.append("activate_delete_builder_query", true);
            }
            else{
                var data   = new FormData(jQuery("#form_query_buider")[0]);
                data.append("activate_builder_query", true);
            }
            data.append("query_name", jQuery('#field_new_query_file_name').val());
            data.append("field_database_name", jQuery('#field_database_name').val());
            data.append("field_query_file_type", queryType);
            jQuery.ajax({
                cache: false,
                async: true,
                type: "POST",
                url: url,
                data: data,
                contentType: false,
                processData: false,
                success: function (data) {
//                    window.location.reload();
                    var beautify 		= ace.require("ace/ext/beautify"); // get reference to extension
                    var editor_query              = ace.edit("editor_query");
                    editor_query.getSession().setValue(data);
                    beautify.beautify(editor_query.session);
                }
            });
        });
        jQuery(document).on('click', '#btn_form_query_builder_clear', function(event) {
            var url 	= '/incoming-data.php';
//            var url 	= '../../actions/action_clear_all.php';

            jQuery('#form_query_buider')[0].reset();

            var data   = new FormData();
            data.append("activate_builder_query_clear_all", true);
            jQuery.ajax({
                cache: false,
                async: true,
                type: "POST",
                url: url,
                data: data,
                contentType: false,
                processData: false,
                success: function (data) {
                    location.reload();
                }
            });
        });
    });
    <!--============My New JS starts from here====================-->
    function loadQuery(queryId){                                //function to be called on change of query_id dropdown
//                var url 	= '/incoming-data.php';
        var url 	= '../../actions/action5_query_load.php';
        var data   = new FormData();
        data.append("activate_builder_query_load", true);
        data.append("queryId",queryId);
        // jquery ajax request to store query Id to the $_SESSION
        jQuery.ajax({
            cache: false,
            async: true,
            type: "POST",
            url: url,
            data: data,
            contentType: false,
            processData: false,
            success: function (data) {
                window.location.reload();
            },
            error:function(e){
                console.log("Something went wrong.");
            }
        });
    }
    function queryTypeChanged(queryType) {// function to hide or show the join table fields according to the query type
        if(queryType=="Single Select" || queryType == "Join Select"){
            jQuery('.selectQueryPart').removeClass('hidden');
            jQuery('.insertQueryPart').addClass('hidden');
        }
        if(queryType == "Insert"){
            jQuery('.selectQueryPart').addClass('hidden');
            jQuery('.insertQueryPart').removeClass('hidden');
        }
        if(queryType=="Single Select"){
            jQuery('.joinSelect').hide();
        }else{
            jQuery('.joinSelect').show();
        }
    }
    //    ===================START JS for Insert Query=====================
    function tableColumnsInsert(tableName,tableSelectObject){
        var field_database_name 	= jQuery('#field_database_name').val();
        var url 			= '/incoming-data.php';
//        var url 			= '../../actions/action3_list_database_table_columns.php';
        var data1   = new FormData();
        data1.append("activate_get_database_table_columns", true);
        data1.append("field_database_name", field_database_name);
        data1.append("tableName", tableName);
        jQuery.ajax({
            cache: false,
            async: true,
            type: "POST",
            url: url,
            data: data1,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function (data) {
                var html='<hr> <div class="col-sm-6 col-md-6 form-group"><label>Column Name</label></div><div class="col-sm-6 col-md-6 form-group"><label>Set a Value</label></div>';
                jQuery.each(data, function(index,column) {
                    html += '<div class="col-sm-6 col-md-6 form-group">' +
                        '<input type="text" class="form-control" readonly value="'+column+'">' +
                        '</div> ' +
                        '<div class="col-sm-6 col-md-6 form-group"> ' +
                        '<input type="text" class="form-control" name="'+tableName+'____'+column+'" placeholder="set a value"> ' +
                        '</div>';
                });
                var parent_container = jQuery(tableSelectObject).closest('.form-group');
                var mainDiv=jQuery(parent_container).closest('.mainDiv');
                jQuery(mainDiv).find('.tableInsertColumnsDiv').html(html);
            },
            error:function(e){
                console.log(e);
            }
        });
    }
    //    ===================END JS for Insert Query=====================
</script>