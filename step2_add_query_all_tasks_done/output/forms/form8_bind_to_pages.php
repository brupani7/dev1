<div class="panel">
          <div class="panel-heading" role="tab">
	          <?php
	          if(isset($queryPrefillData)) {
	          ?>
              <a class="panel-title" data-parent="#accordion_build_query" data-toggle="collapse" href="#tab_add_pages" aria-controls="tab_add_pages" aria-expanded="true">
                  Assign To Pages
              </a>
          </div>
            <div class="panel-collapse collapse in" id="tab_add_pages" role="tabpanel" aria-expanded="true">
                <?php
                }else{
                ?>
                <a class="panel-title collapsed" data-parent="#accordion_build_query" data-toggle="collapse" href="#tab_add_pages" aria-controls="tab_add_pages" aria-expanded="false">
                    Assign To Pages
                </a>
            </div>
            <div class="panel-collapse collapse" id="tab_add_pages" role="tabpanel">
                <?php
                }
                ?>
                    <div class="panel-body duplicate_append_to">
                              <div class="row">
                                        <div class="col-md-12 duplicate_append_to_inner">
                                            <?php
                                            $pagesResult=mysqli_query($connection,"SELECT page_id FROM builder6_canvas_query_bind_to_pages WHERE query_id=".$_SESSION['query_id']) or die(mysqli_error($connection));
                                            if(mysqli_num_rows($pagesResult) > 0){
                                                while($page=mysqli_fetch_assoc($pagesResult)){
                                                    ?>
                                                    <div class="row table_row">
                                                        <div class="form-group col-12">
                                                            <div class="list-group">
                                                                <select name="query_assign_to_pages[]" class=" form-control">
					                                                <?php foreach ($array_site_pages as $key): ?>
                                                                        <option <?php if($key['page_id'] == $page['page_id']){ echo "selected";} ?> value="<?php echo $key['page_id'] ?>"><?php echo $key['page_name'] ?></option>
					                                                <?php endforeach; ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            else{
	                                            ?>
                                                <div class="row table_row">
                                                    <div class="form-group col-12">
                                                        <div class="list-group">
                                                            <select name="query_assign_to_pages[]" class=" form-control">
					                                            <?php foreach ($array_site_pages as $key): ?>
                                                                    <option value="<?php echo $key['page_id'] ?>"><?php echo $key['page_name'] ?></option>
					                                            <?php endforeach; ?>
                                                            </select>
                                                        </div>
                                                        <a href="#" class="table_page_remove">Remove</a>
                                                    </div>
                                                </div>
	                                            <?php
                                            }
                                            ?>
                                        </div>
                              </div>
                              <div class="row">
				<div class="col-12">
					<a href="#" class="table_page_remove">Remove</a>
					<button type="button" class="duplicate_row btn btn-primary waves-effect waves-classic">Add Another</button>
				</div>
			</div>
                    </div>
          </div>
</div>
