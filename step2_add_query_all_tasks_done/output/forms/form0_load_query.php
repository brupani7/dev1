<div class="panel">
          <div class="panel-heading" role="tab">
                    <a class="panel-title collapsed" data-parent="#accordion_build_query" data-toggle="collapse" href="#tab_load_query" aria-controls="tab_load_query" aria-expanded="false">
                             New Query / Load Query
                    </a>
          </div>
          <div class="panel-collapse collapse" id="tab_load_query" role="tabpanel">
                    <div class="panel-body">
                              <div class="row">
                                        <div class="form-group col-md-12">
                                                  <label>New Query Name</label>
                                                  <input class="form-control" placeholder="Enter the Query Name" name="query_name" id="query_name" required>
                                        </div>
                              </div>
                              <div class="row">
                                        <div  class="col-md-12 ">
                                                  <div class="row">
                                                            <div class="form-group col-12">
                                                            <select id="" name="" class="form-control">
                                                                      <option value="" selected disabled>Existing Queries</option>
                                                            </select>
                                                  </div>
                                                  <div class="form-group col-12">
                                                            <button id="" type="button" class="btn btn-primary waves-effect waves-classic">Load Query</button>
                                                  </div>

                                                  </div>
                                        </div>
                              </div>
                    </div>
          </div>
</div>
