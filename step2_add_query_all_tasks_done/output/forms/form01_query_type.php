<div class="panel">

	<div class="panel-heading" role="tab">
		<?php
		if(isset($queryPrefillData) && !empty($queryPrefillData['field_query_file_type'])) {
		?>
        <a class="panel-title" data-parent="#accordion_build_query" data-toggle="collapse" href="#tab_add_query_type" aria-controls="tab_add_query_type" aria-expanded="true">
            Add Query Type
        </a>
    </div>
    <div class="panel-collapse collapse in" id="tab_add_query_type" role="tabpanel" aria-expanded="true">
		<?php
		}else{
		?>
        <a class="panel-title collapsed" data-parent="#accordion_build_query" data-toggle="collapse" href="#tab_add_query_type" aria-controls="tab_add_query_type" aria-expanded="false">
            Add Query Type
        </a>
    </div>
    <div class="panel-collapse collapse" id="tab_add_query_type" role="tabpanel" style="">
		<?php
		}
		?>
		<div class="panel-body">
			<div class="row">
				<div  class="col-md-12 duplicate_append_to_inner">
					<div class="row">
						<div class="form-group col-12">
							<select id="field_query_file_type" name="field_query_file_type" class="form-control" onchange="queryTypeChanged(this.value)">
								<option selected disabled value="">Select Query Type</option>
                                <option  <?php if(isset($queryPrefillData) && isset($queryPrefillData['field_query_file_type']) && $queryPrefillData['field_query_file_type'] == "Single Select"){ echo "selected"; } ?> value="Single Select">Single Select</option>
                                <option  <?php if(isset($queryPrefillData) && isset($queryPrefillData['field_query_file_type']) && $queryPrefillData['field_query_file_type'] == "Join Select"){ echo "selected"; } ?> value="Join Select">Join Select</option>
                                <option  <?php if(isset($queryPrefillData) && isset($queryPrefillData['field_query_file_type']) && $queryPrefillData['field_query_file_type'] == "Update"){ echo "selected"; } ?> disabled value="Update">Update</option>
								<option  <?php if(isset($queryPrefillData) && isset($queryPrefillData['field_query_file_type']) && $queryPrefillData['field_query_file_type'] == "Insert"){ echo "selected"; } ?> disabled value="Insert">Insert</option>
								<option  <?php if(isset($queryPrefillData) && isset($queryPrefillData['field_query_file_type']) && $queryPrefillData['field_query_file_type'] == "Delete"){ echo "selected"; } ?> disabled value="Delete">Delete</option>
							</select>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
