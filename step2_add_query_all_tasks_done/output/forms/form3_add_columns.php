<div class="panel">
            <div class="panel-heading" role="tab">
              <?php
              if(isset($queryPrefillData) && !empty($queryPrefillData['field_select_column_names'])) {
	              ?>
                  <a class="panel-title" data-parent="#accordion_build_query" data-toggle="collapse"
                     href="#tab_add_fields" aria-controls="tab_add_fields" aria-expanded="true">
                      Select Columns
                  </a>
            </div>
            <div class="panel-collapse collapse in" id="tab_add_fields" role="tabpanel" aria-expanded="true">
	              <?php
              }else{
	              ?>
                  <a class="panel-title collapsed" data-parent="#accordion_build_query" data-toggle="collapse"
                     href="#tab_add_fields" aria-controls="tab_add_fields" aria-expanded="false">
                      Select Columns
                  </a>
            </div>
            <div class="panel-collapse collapse" id="tab_add_fields" role="tabpanel">
	              <?php
              }
              ?>
                    <div class="panel-body duplicate_append_to">
                        <div class="row">
                            <div class="col-md-12 duplicate_append_to_inner">

                        <?php
                        if(isset($queryPrefillData) && !empty($queryPrefillData['field_select_column_names'])){
                            for($i=0;$i<count($queryPrefillData['field_select_column_names']);$i++){
                                ?>
                                        <div class="row table_row">
                                            <div class="form-group col-6">
                                                <div class="list-group">
                                                    <select name="field_select_column_names[]" class="fill_with_all_table_columns form-control">
                                                        <option selected disabled value="">Select Column</option>
							                            <?php
							                            foreach ($database_table_columns as $database_table_column){?>
                                                            <option <?php if(isset($queryPrefillData['field_select_column_names'][$i]) && $queryPrefillData['field_select_column_names'][$i]== $database_table_column){ echo "selected"; } ?> value="<?php echo $database_table_column; ?>"><?php echo $database_table_column; ?></option>
							                            <?php }
							                            ?>
                                                    </select>
                                                </div>
                                                <a href="#" class="table_row_delete">Delete Column</a>
                                            </div>
                                            <div class="form-group col-6">
                                                <input type="text" name="field_select_column_names_as[]" class="form-control" placeholder="AS" value="<?php if(isset($queryPrefillData['field_select_column_names_as'][$i]) && $queryPrefillData['field_select_column_names_as'][$i] != ""){ echo $queryPrefillData['field_select_column_names_as'][$i]; } ?>">
                                            </div>
                                        </div>

                                <?php
                            }
                        }
                        else{
                            ?>

                                    <div class="row table_row">
                                        <div class="form-group col-6">
                                            <div class="list-group">
                                                <select name="field_select_column_names[]" class="fill_with_all_table_columns form-control">
                                                    <option selected disabled value="">Select Column</option>
                                                </select>
                                            </div>
                                            <a href="#" class="table_row_delete">Delete Column</a>
                                        </div>
                                        <div class="form-group col-6">
                                            <input type="text" name="field_select_column_names_as[]" class="form-control" placeholder="AS" value="">
                                        </div>
                                    </div>
                            <?php
                        }
                        ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <button type="button" class="duplicate_row btn btn-primary waves-effect waves-classic">Add Column</button>
                            </div>
                        </div>
                    </div>
          </div>
</div>
