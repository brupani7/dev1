<div class="panel">
          <div class="panel-heading" role="tab">
	          <?php
	          if(isset($queryPrefillData)) {
	          ?>
              <a class="panel-title" data-parent="#accordion_build_query" data-toggle="collapse" href="#tab_add_limit" aria-controls="tab_add_limit" aria-expanded="true">
                  Add Limit
              </a>
          </div>
            <div class="panel-collapse collapse in" id="tab_add_limit" role="tabpanel" aria-expanded="true">
                <?php
                }else{
                ?>
                <a class="panel-title collapsed" data-parent="#accordion_build_query" data-toggle="collapse" href="#tab_add_limit" aria-controls="tab_add_limit" aria-expanded="false">
                    Add Limit
                </a>
            </div>
            <div class="panel-collapse collapse" id="tab_add_limit" role="tabpanel">
                <?php
                }
                ?>
                    <div class="panel-body">
                              <div class="row">
                                        <div  class="col-md-12 duplicate_append_to_inner">
                                                  <div class="row">
                                                            <div class="form-group col-6">
                                                                      <input name="field_limit_min" type="text" class="form-control" placeholder="LIMIT MIN" value="<?php if(isset($queryPrefillData['field_limit_min']) && $queryPrefillData['field_limit_min'] != ""){ echo $queryPrefillData['field_limit_min']; } ?>">
                                                            </div>
                                                            <div class="form-group col-6">
                                                                      <input name="field_limit_max" type="text" class="form-control" placeholder="LIMIT MAX" value="<?php if(isset($queryPrefillData['field_limit_max']) && $queryPrefillData['field_limit_max'] != ""){ echo $queryPrefillData['field_limit_max']; } ?>">
                                                            </div>

                                                  </div>
                                                  

                                        </div>
                              </div>
                    </div>
          </div>
</div>
