<div class="panel">
	<div class="panel-heading" role="tab">
		<?php
		if(isset($queryPrefillData) && !empty($queryPrefillData['db_table_name1'])) {
			?>
            <a class="panel-title" data-parent="#accordion_build_query" data-toggle="collapse"
               href="#tab_add_tables" aria-controls="tab_add_tables" aria-expanded="true">
                Add Tables
            </a>
        </div>
        <div class="panel-collapse collapse in" id="tab_add_tables" role="tabpanel" aria-expanded="true">
			<?php
		}else{
			?>
            <a class="panel-title collapsed" data-parent="#accordion_build_query" data-toggle="collapse"
               href="#tab_add_tables" aria-controls="tab_add_tables" aria-expanded="false">
                Add Tables
            </a>
        </div>
        <div class="panel-collapse collapse" id="tab_add_tables" role="tabpanel" style="">
			<?php
        }
        ?>
		<div class="panel-body duplicate_append_to">
			<div class="row">
				<div id="table_row_fields" class="col-md-12 duplicate_append_to_inner">
					<div class="row">
						<div class="col-6">
							<div class="form-group">
								<select name="db_table_name1" class="form-control table_name table_name1" onchange="tableColumns1(this,this.value)">
									<option value="" selected disabled>Table</option>
                                    <?php
                                    if(isset($queryPrefillData) && isset($queryPrefillData['db_table_name1']) && !empty($queryPrefillData['field_database_name'])) {
                                        $field_database_name=$queryPrefillData['field_database_name'];
	                                    $result2         = mysqli_query( $connection1, "SHOW TABLES FROM `$field_database_name`" );
	                                    $database_tables = array();
	                                    while ( $row2 = mysqli_fetch_array( $result2 ) ) {
		                                    $database_tables[] = $row2[0];
	                                    }
                                    }
                                    ?>
									<?php foreach ($database_tables as $value): ?>
                                        <option <?php if(isset($queryPrefillData) && isset($queryPrefillData['db_table_name1']) && $queryPrefillData['db_table_name1'] == $value ){ echo "selected"; } ?> value="<?php echo $value ?>"><?php echo $value ?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-6">
							<div class="col-6">
								<div class="form-group">
									<input type="text" name="field_as_table1" class="form-control table_name_as1" placeholder="AS" value="<?php if(isset($queryPrefillData) && isset($queryPrefillData['field_as_table1'])){ echo $queryPrefillData['field_as_table1']; } ?>" onchange="tableColumnsAs1(this,this.value)">
								</div>
							</div>
						</div>
					</div>
                    <?php
                    if(isset($queryPrefillData)){
	                    $database_table_columns = array();
	                    $d                      = array();
	                    $tableName=$queryPrefillData['db_table_name1'];

	                    $query  = "SHOW COLUMNS FROM `$tableName` FROM `$field_database_name`";
	                    $result = mysqli_query( $connection1, $query ) or die(mysqli_error($connection1));
	                    while ( $row = mysqli_fetch_array( $result ) ) {
		                    if(isset($queryPrefillData['field_as_table1']) && !empty($queryPrefillData['field_as_table1'])){
			                    if(!in_array($queryPrefillData['field_as_table1'].".".$row[0],$database_table_columns)) {
				                    $database_table_columns[] = $queryPrefillData['field_as_table1'] . "." . $row[0];
			                    }
		                    }
		                    else{
			                    if(!in_array($tableName.".".$row[0],$database_table_columns)) {
				                    $database_table_columns[] = $tableName . "." . $row[0];
			                    }
		                    }
	                    }
                        if(isset($queryPrefillData['field_query_file_type']) && $queryPrefillData['field_query_file_type'] == "Single Select"){
                            ?>
                            <div class="row row_master table_row joinSelect" style="display: none;">
                                <div class="form-group col-2">
                                    <select name="field_join_type[]" class="form-control joinSelect" id="field_join_type">
                                        <option value="" disabled selected>Join Type</option>
                                        <option  value="INNER JOIN">Inner Type</option>
                                        <option value="LEFT JOIN">Left Type</option>
                                        <option  value="RIGHT JOIN">Right Type</option>
                                    </select>
                                </div>
                                <div class="form-group col-2">
                                    <select name="db_table_name2[]" id="db_table_name2" class="form-control table_name table_name2 joinSelect" onchange="tableColumns2(this,this.value)">
                                        <option value="" selected disabled>Table</option>
                                    </select>
                                </div>
                                <div class="form-group col-2">
                                    <input type="text" name="field_as_table2[]" id="field_as_table2" class="form-control table_name_as2 joinSelect" placeholder="AS" value="" onchange="tableColumnsAs2(this,this.value)">
                                </div>
                                <div class="form-group col-2">
                                    <select name="field_table_on_1[]" id="field_table_on_1" class="form-control  fill_with_all_table_columns1 joinSelect">
                                        <option value="" selected disabled>ON 1</option>
                                    </select>
                                </div>
                                <div class="form-group col-2">
                                    <select name="field_table_condition[]" id="field_table_condition" class="form-control joinSelect">
                                        <option value="" selected disabled>CONDITION</option>
                                        <option  value="=">  =  </option>
                                        <option  value="!="> != </option>
                                        <option value="<">  <  </option>
                                        <option value=">">  >  </option>
                                        <option  value=">="> >= </option>
                                        <option  value="<="> <= </option>
                                    </select>
                                </div>
                                <div class="form-group col-2">
                                    <select name="field_table_on2[]" id="field_table_on2" class="form-control fill_with_all_table_columns2 joinSelect">
                                        <option value="" selected disabled>ON 2</option>
                                    </select>
                                    <a href="#" class="table_row_delete">Delete Row</a>
                                </div>

                            </div>
                            <?php
                        }
                        else{
	                        if($queryPrefillData['field_join_type'] != "" && !empty($queryPrefillData['field_join_type'])){
		                        if(count($queryPrefillData['field_join_type']) > 0){

			                        for($i=0;$i<count($queryPrefillData['field_join_type']);$i++) {
				                        $tableName=$queryPrefillData['db_table_name2'][$i];
				                        $query = "SHOW COLUMNS FROM `$tableName` FROM `$field_database_name`";
				                        $result = mysqli_query( $connection1, $query ) or die( mysqli_error( $connection1 ) );
				                        while ( $row = mysqli_fetch_array( $result ) ) {
					                        if(isset($queryPrefillData['field_as_table2'][$i]) && !empty($queryPrefillData['field_as_table2'][$i])){
						                        if(!in_array($queryPrefillData['field_as_table2'][$i].".".$row[0],$database_table_columns)) {
							                        $database_table_columns[] = $queryPrefillData['field_as_table2'][$i] . "." . $row[0];
						                        }
					                        }
					                        else{
						                        if(!in_array($tableName.".".$row[0],$database_table_columns)) {
							                        $database_table_columns[] = $tableName . "." . $row[0];
						                        }
					                        }
				                        }
			                        }

			                        for($i=0;$i<count($queryPrefillData['field_join_type']);$i++){
				                        ?>
                                        <div class="row row_master table_row">
                                            <div class="form-group col-2">
                                                <select name="field_join_type[]" class="form-control joinSelect" id="field_join_type">
                                                    <option value="" disabled selected>Join Type</option>
                                                    <option <?php if(isset($queryPrefillData) && isset($queryPrefillData['field_join_type'][$i]) && $queryPrefillData['field_join_type'][$i] == "INNER JOIN"){ echo "selected"; } ?> value="INNER JOIN">Inner Type</option>
                                                    <option <?php if(isset($queryPrefillData) && isset($queryPrefillData['field_join_type'][$i]) && $queryPrefillData['field_join_type'][$i] == "LEFT JOIN"){ echo "selected"; } ?> value="LEFT JOIN">Left Type</option>
                                                    <option <?php if(isset($queryPrefillData) && isset($queryPrefillData['field_join_type'][$i]) && $queryPrefillData['field_join_type'][$i] == "RIGHT JOIN"){ echo "selected"; } ?> value="RIGHT JOIN">Right Type</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-2">
                                                <select name="db_table_name2[]" id="db_table_name2" class="form-control table_name table_name2 joinSelect" onchange="tableColumns2(this,this.value)">
                                                    <option value="" selected disabled>Table</option>
							                        <?php foreach ($database_tables as $value): ?>
                                                        <option <?php if(isset($queryPrefillData) && isset($queryPrefillData['db_table_name2'][$i]) && $queryPrefillData['db_table_name2'][$i] == $value ){ echo "selected"; } ?> value="<?php echo $value ?>"><?php echo $value ?></option>
							                        <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-2">
                                                <input type="text" name="field_as_table2[]" id="field_as_table2" class="form-control table_name_as2 joinSelect" placeholder="AS" value="<?php if(isset($queryPrefillData) && isset($queryPrefillData['field_as_table2'][$i])){ echo $queryPrefillData['field_as_table2'][$i]; } ?>" onchange="tableColumnsAs2(this,this.value)">
                                            </div>
                                            <div class="form-group col-2">
                                                <select name="field_table_on_1[]" id="field_table_on_1" class="form-control  fill_with_all_table_columns1 joinSelect">
                                                    <option value="" selected disabled>ON 1</option>
							                        <?php
							                        foreach ($database_table_columns as $database_table_column){?>
                                                        <option <?php if(isset($queryPrefillData['field_table_on_1'][$i]) && $queryPrefillData['field_table_on_1'][$i]== $database_table_column){ echo "selected"; } ?> value="<?php echo $database_table_column; ?>"><?php echo $database_table_column; ?></option>
							                        <?php }
							                        ?>
                                                </select>
                                            </div>
                                            <div class="form-group col-2">
                                                <select name="field_table_condition[]" id="field_table_condition" class="form-control joinSelect">
                                                    <option value="" selected disabled>CONDITION</option>
                                                    <option <?php if(isset($queryPrefillData['field_table_condition'][$i]) && $queryPrefillData['field_table_condition'][$i]== "= "){ echo "selected"; } ?> value="=">  =  </option>
                                                    <option <?php if(isset($queryPrefillData['field_table_condition'][$i]) && $queryPrefillData['field_table_condition'][$i]== "!="){ echo "selected"; } ?> value="!="> != </option>
                                                    <option <?php if(isset($queryPrefillData['field_table_condition'][$i]) && $queryPrefillData['field_table_condition'][$i]== "<"){ echo "selected"; } ?> value="<">  <  </option>
                                                    <option <?php if(isset($queryPrefillData['field_table_condition'][$i]) && $queryPrefillData['field_table_condition'][$i]== ">"){ echo "selected"; } ?> value=">">  >  </option>
                                                    <option <?php if(isset($queryPrefillData['field_table_condition'][$i]) && $queryPrefillData['field_table_condition'][$i]== ">="){ echo "selected"; } ?> value=">="> >= </option>
                                                    <option <?php if(isset($queryPrefillData['field_table_condition'][$i]) && $queryPrefillData['field_table_condition'][$i]== "<="){ echo "selected"; } ?> value="<="> <= </option>
                                                </select>
                                            </div>
                                            <div class="form-group col-2">
                                                <select name="field_table_on2[]" id="field_table_on2" class="form-control fill_with_all_table_columns2 joinSelect">
                                                    <option value="" selected disabled>ON 2</option>
							                        <?php
							                        foreach ($database_table_columns as $database_table_column){?>
                                                        <option <?php if(isset($queryPrefillData['field_table_on2'][$i]) && $queryPrefillData['field_table_on2'][$i]== $database_table_column){ echo "selected"; } ?> value="<?php echo $database_table_column; ?>"><?php echo $database_table_column; ?></option>
							                        <?php }
							                        ?>
                                                </select>
                                                <a href="#" class="table_row_delete">Delete Row</a>
                                            </div>

                                        </div>
				                        <?php
			                        }
		                        }
	                        }
                        }
                    }
                    else {
	                    ?>
                        <div class="row row_master table_row joinSelect">
                            <div class="form-group col-2">
                                <select name="field_join_type[]" class="form-control joinSelect" id="field_join_type">
                                    <option value="" disabled selected>Join Type</option>
                                    <option  value="INNER JOIN">Inner Type</option>
                                    <option value="LEFT JOIN">Left Type</option>
                                    <option  value="RIGHT JOIN">Right Type</option>
                                </select>
                            </div>
                            <div class="form-group col-2">
                                <select name="db_table_name2[]" id="db_table_name2"
                                        class="form-control table_name table_name2 joinSelect"
                                        onchange="tableColumns2(this,this.value)">
                                    <option value="" selected disabled>Table</option>
                                </select>
                            </div>
                            <div class="form-group col-2">
                                <input type="text" name="field_as_table2[]" id="field_as_table2"
                                       class="form-control table_name_as2 joinSelect" placeholder="AS" value=""
                                       onchange="tableColumnsAs2(this,this.value)">
                            </div>
                            <div class="form-group col-2">
                                <select name="field_table_on_1[]" id="field_table_on_1"
                                        class="form-control  fill_with_all_table_columns1 joinSelect">
                                    <option value="" selected disabled>ON 1</option>
                                </select>
                            </div>
                            <div class="form-group col-2">
                                <select name="field_table_condition[]" id="field_table_condition"
                                        class="form-control joinSelect">
                                    <option value="" selected disabled>CONDITION</option>
                                    <option value="="> =</option>
                                    <option value="!="> !=</option>
                                    <option value="<"> <</option>
                                    <option value=">"> ></option>
                                    <option value=">="> >=</option>
                                    <option value="<="> <=</option>
                                </select>
                            </div>
                            <div class="form-group col-2">
                                <select name="field_table_on2[]" id="field_table_on2"
                                        class="form-control fill_with_all_table_columns2 joinSelect">
                                    <option value="" selected disabled>ON 2</option>
                                </select>
                                <a href="#" class="table_row_delete">Delete Row</a>
                            </div>

                        </div>
	                    <?php
                    }
                    ?>
				</div>
			</div>
            <?php
            if(isset($queryPrefillData) && isset($queryPrefillData['field_query_file_type']) && $queryPrefillData['field_query_file_type'] == "Single Select"){
                ?>
                <div class="row joinSelect" style="display: none;">
                    <div class="col-12">
                        <button type="button" class="duplicate_row btn btn-primary waves-effect waves-classic float-right">Add Another Table</button>
                    </div>
                </div>
                <?php
            }
            else{
	            ?>
                <div class="row joinSelect">
                    <div class="col-12">
                        <button type="button" class="duplicate_row btn btn-primary waves-effect waves-classic float-right">Add Another Table</button>
                    </div>
                </div>
	            <?php
            }
            ?>
		</div>
	</div>
</div>
