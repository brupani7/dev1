<?php
//include "config.php";
//session_start();
$project_id = $_SESSION['project_id'];
$section_id = $_SESSION['section_id'];
$page_id    = $_SESSION['page_id'];
$query_id   = $_SESSION['query_id'];

$query_get_save_locations    = "SELECT
`t1`.`project_directory`,
`t2`.`section_name`,
`t2`.`page_template_file`
FROM `builder0_projects` as `t1`
INNER JOIN `builder1_section_new` as `t2`
ON  `t1`.`project_id` = `t2`.`project_id` WHERE `t1`.`project_id`=? AND `t2`.`section_id` = ?";
if ($stmt_get_save_locations = $connection->prepare($query_get_save_locations)) {
	$stmt_get_save_locations->bind_param('ii',$project_id,$section_id);

	if(!$stmt_get_save_locations->execute()){
		echo $stmt_get_save_locations->error;
		$stmt_get_save_locations->free_result();
		$stmt_get_save_locations->close();
		exit();
	}else {
		$stmt_get_save_locations->store_result();
		$total_result = $stmt_get_save_locations->num_rows;
		if ($total_result > 0 ) {
			$stmt_get_save_locations->bind_result($project_directory,$section_name,$page_template_file);
			$stmt_get_save_locations->fetch();

		}
	}
}


if(!empty($_POST)) {
	/* Fetch all post parameters and set them to "" (null) if its not set in the post */
	$query_name                          = str_replace(" ","_",str_replace("-","_",(isset( $_POST['query_name']) ? $_POST['query_name'] : "" )));
	$field_database_name                 = (isset( $_POST['field_database_name']) ? $_POST['field_database_name'] : "" );
	$field_query_type                    = (isset( $_POST['field_query_file_type']) ? $_POST['field_query_file_type'] : "" );
	$table_name                          = (isset( $_POST['table_name']) ? $_POST['table_name'] : "" );

	$query_array = array(
		array(
			'query_name' =>				$_POST['query_name'],
			'field_database_name' =>			$_POST['field_database_name'],
			'field_query_file_type' =>			$_POST['field_query_file_type'],
			'table_name' =>			$_POST['table_name']
		)
	);

	$query                               = "";
	$insert                              = "";
	$bindParam=[];
	$bindParamNew=[];
	$bindS="";
	$query  = "SHOW COLUMNS FROM `".$table_name."` FROM `$field_database_name`";
	$result = mysqli_query( $connection1, $query ) or die(mysqli_error($connection1));
	$i=1;
	$query='INSERT INTO `'.$field_database_name.'`.`'.$table_name.'` ';
	$bindParamString="";
	while ( $row = mysqli_fetch_array( $result ) ) {
		if ( isset( $_POST[ $table_name . "____" . $row[0] ] ) && $_POST[ $table_name . "____" . $row[0] ] != "" ) {
			if ( $i == 1 ) {
				$query .= '(`' . $row[0].'`' ;
				$bindParamString .= '( ?';
			} else {
				$query .= '`'.$row[0].'`';
				$bindParamString .= '?';
			}
			if ( $i == mysqli_num_rows( $result ) ) {
				$query .= ") ";
				$bindParamString .= ") ";
			} else {
				$query.=", ";
				$bindParamString.=", ";
			}
			$bindParam[]= $_POST[ $table_name . "____" . $row[0] ];
			$query_array[0]['columns'][$table_name][$row[0]]= $_POST[ $table_name . "____" . $row[0] ];
			$i ++;
		}
	}
	$query.="VALUES ".$bindParamString;
	$subQueryString="";
	for($i=1;$i<=count($bindParam);$i++){
		$subQueryString.='$stmt_' . $query_name . '->bindParam('.$i.','.$bindParam[$i].') <br>';
	}
		// if sql query is not empty
		// start writing final query string variable to display in output, and append all the variables like bind parameters and bind ssss and the query name
		$queryString = '
$query_' . $query_name . '= "' . $query . '";

$stmt_' . $query_name . ' = $connection->prepare($query_' . $query_name . ');
'.$subQueryString.'

if(!$stmt_' . $query_name . '->execute()){
	echo $stmt_' . $query_name . '->error;
	$stmt_' . $query_name . '->free_result();
	$stmt_' . $query_name . '->close();
	exit(\'failed - ' . $query_name . '\');
}else {
	echo "inserted successfully";
}';
	echo $queryString; // display $queryString variable

	$query_update_original    = "DELETE FROM `builder6_canvas_query` WHERE `query_id`=?";

	if ($stmt_update_original = $connection->prepare($query_update_original)) {
		$stmt_update_original->bind_param('i',$query_id);

		if(!$stmt_update_original->execute()){
			echo $stmt_update_original->error;
			$stmt_update_original->free_result();
			$stmt_update_original->close();
			exit();
		}else {
			$new_query_id = $stmt_update_original->insert_id;
		}
	}else {
		echo 'prepare failed - insert query 1';
	}
	if ($query_id > 0) {
		$query_save_query_db    = "INSERT INTO `builder6_canvas_query`(`query_id`, `query_type`, `query_name`, `full_query`, `query_parts_prefill`,`project_id`) VALUES (?,?,?,?,?,?)";
	}else {
		$query_save_query_db    = "INSERT INTO `builder6_canvas_query`(`query_id`, `query_type`, `query_name`, `full_query`, `query_parts_prefill`,`project_id`) VALUES (default,?,?,?,?,?)";
	}
	if ($stmt_save_query_db = $connection->prepare($query_save_query_db)) {
		if ($query_id > 0) {
			$stmt_save_query_db->bind_param('issssi',$query_id,$query_type,$query_name,$full_query,$query_parts_prefill,$project_id);
		}else {
			$stmt_save_query_db->bind_param('ssssi',$query_type,$query_name,$full_query,$query_parts_prefill,$project_id);
		}

		$query_type	 = $_POST['field_query_file_type'];
		$query_name	 =  $query_array[0]['query_name'];
		$full_query	 = $queryString;
		$query_parts_prefill = json_encode($query_array);
		$page_id	 	 = $_SESSION['page_id'];
		$section_id	 = $_SESSION['section_id'];
		$project_id	 = $_SESSION['project_id'];

		if(!$stmt_save_query_db->execute()){
			echo $stmt_save_query_db->error;
			$stmt_save_query_db->free_result();
			$stmt_save_query_db->close();
			exit();
		}else {
			$new_query_id = $stmt_save_query_db->insert_id;
		}
	}else {
		echo 'prepare failed - insert query 2';
	}

	// create directory
	if(!file_exists($_SERVER['DOCUMENT_ROOT'] . '/projects' . '/' . $project_directory . '/' . $section_name . '/queries')){
		mkdir($_SERVER['DOCUMENT_ROOT'] . '/projects' . '/' . $project_directory . '/' . $section_name . '/queries',0777);
	}
	// delete old file
	if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/projects' . '/' . $project_directory . '/' . $section_name . '/queries' . '/' . $query_name . '.php')) {
		unlink($_SERVER['DOCUMENT_ROOT'] . '/projects' . '/' . $project_directory . '/' . $section_name . '/queries' . '/' . $query_name . '.php');
	}
	// create new file
	$query_template_file   = $_SERVER['DOCUMENT_ROOT'] . '/projects' . '/' . $project_directory . '/' . $section_name . '/queries' . '/' . $query_name . '.php';

	$query_file = fopen($query_template_file, "w");
	fwrite($query_file, $queryString);
	fclose($query_file);
	$_SESSION['query_id'] = $new_query_id;


}
?>
