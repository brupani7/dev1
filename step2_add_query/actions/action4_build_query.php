<?php

$project_id = $_SESSION['project_id'];
$section_id = $_SESSION['section_id'];
$page_id    = $_SESSION['page_id'];
$query_id   = $_SESSION['query_id'];

$query_get_save_locations    = "SELECT
`t1`.`project_directory`,
`t2`.`section_name`,
`t2`.`page_template_file`
FROM `builder0_projects` as `t1`
INNER JOIN `builder1_section_new` as `t2`
ON  `t1`.`project_id` = `t2`.`project_id` WHERE `t1`.`project_id`=? AND `t2`.`section_id` = ?";
if ($stmt_get_save_locations = $connection->prepare($query_get_save_locations)) {
	$stmt_get_save_locations->bind_param('ii',$project_id,$section_id);

	if(!$stmt_get_save_locations->execute()){
		echo $stmt_get_save_locations->error;
		$stmt_get_save_locations->free_result();
		$stmt_get_save_locations->close();
		exit();
	}else {
		$stmt_get_save_locations->store_result();
		$total_result = $stmt_get_save_locations->num_rows;
		if ($total_result > 0 ) {
			$stmt_get_save_locations->bind_result($project_directory,$section_name,$page_template_file);
			$stmt_get_save_locations->fetch();

		}
	}
}


if(!empty($_POST)) {
	/* Fetch all post parameters and set them to "" (null) if its not set in the post */
	$query_name                          = (isset( $_POST['query_name']) ? $_POST['query_name'] : "" );
	$field_database_name                 = (isset( $_POST['field_database_name']) ? $_POST['field_database_name'] : "" );
	$field_join_type                     = (isset( $_POST['field_join_type']) ? $_POST['field_join_type'] : "" );
	$db_table_name1                      = (isset( $_POST['db_table_name1']) ? $_POST['db_table_name1'] : "" );
	$db_table_name2                      = (isset( $_POST['db_table_name2']) ? $_POST['db_table_name2'] : "" );
	$field_as_table1                     = (isset( $_POST['field_as_table1']) ? $_POST['field_as_table1'] : "" );
	$field_as_table2                     = (isset( $_POST['field_as_table2']) ? $_POST['field_as_table2'] : "" );
	$field_table_on_1                    = (isset( $_POST['field_table_on_1']) ? $_POST['field_table_on_1'] : "" );
	$field_table_condition               = (isset( $_POST['field_table_condition']) ? $_POST['field_table_condition'] : "" );
	$field_table_on2                     = (isset( $_POST['field_table_on2']) ? $_POST['field_table_on2'] : "" );
	$field_select_column_names           = (isset( $_POST['field_select_column_names']) ? $_POST['field_select_column_names'] : "" );
	$field_select_column_names_as        = (isset( $_POST['field_select_column_names_as']) ? $_POST['field_select_column_names_as'] : "" );
	$field_where_columns                 = (isset( $_POST['field_where_columns'] ) ? $_POST['field_where_columns'] : "" );
	$field_where_columns_conditions      = (isset( $_POST['field_where_columns_conditions'] ) ? $_POST['field_where_columns_conditions'] : "" );
	$field_where_columns_compare_columns = (isset( $_POST['field_where_columns_compare_columns'] ) ? $_POST['field_where_columns_compare_columns'] : "" );
	$field_where_columns_compare_values  = (isset( $_POST['field_where_columns_compare_values'] ) ? $_POST['field_where_columns_compare_values'] : "" );
	$field_group_by_columns_selected     = (isset( $_POST['field_group_by_columns_selected'] ) ? $_POST['field_group_by_columns_selected'] : "" );
	$field_order_by_columns_selected     = (isset( $_POST['field_order_by_columns_selected'] ) ? $_POST['field_order_by_columns_selected'] : "" );
	$field_limit_min                     = (isset( $_POST['field_limit_min'] ) ? $_POST['field_limit_min'] : "" );
	$field_limit_max                     = (isset( $_POST['field_limit_max'] ) ? $_POST['field_limit_max'] : "" );

	$array_query_page_list	       = $_POST['array_page_list'];

	$query_array = array(
		array(
			'query_name' =>				$_POST['query_name'],
			'field_database_name' =>			$_POST['field_database_name'],
			'field_join_type' =>			$_POST['field_join_type'],
			'db_table_name1' =>				$_POST['db_table_name1'],
			'db_table_name2' =>				$_POST['db_table_name2'],
			'field_as_table1' =>			$_POST['field_as_table1'],
			'field_as_table2' =>			$_POST['field_as_table2'],
			'field_table_on_1' =>			$_POST['field_table_on_1'],
			'field_table_condition' =>			$_POST['field_table_condition'],
			'field_table_on2' =>			$_POST['field_table_on2'],
			'field_select_column_names' =>		$_POST['field_select_column_names'],
			'field_select_column_names_as' =>		$_POST['field_select_column_names_as'],
			'field_where_columns' =>			$_POST['field_where_columns'],
			'field_where_columns_conditions' =>		$_POST['field_where_columns_conditions'],
			'field_where_columns_compare_columns' =>	$_POST['field_where_columns_compare_columns'],
			'field_where_columns_compare_values' =>		$_POST['field_where_columns_compare_values'],
			'field_group_by_columns_selected' =>		$_POST['field_group_by_columns_selected'],
			'field_order_by_columns_selected' =>		$_POST['field_order_by_columns_selected'],
			'field_limit_min' =>		 	$_POST['field_limit_min'],
			'field_limit_max' =>			$_POST['field_limit_max']
		)
	);

	$query                               = "";
	$select                              = "";
	$bindParam=[];
	$bindS="";
	if($field_join_type != "") {                                                                                // if $field_join_type is not blank
		for ( $c = 0; $c < count( $field_select_column_names ); $c ++ ) {                                       // loop through the all column names selected in select_column dropdown
			$column=str_replace(".","_",$field_select_column_names[ $c ]);                       // replace _ with  . from the column names
			$select .= "`".str_replace(".","`.`",$field_select_column_names[ $c ])."`";          // put `` for each column names
			if ( isset( $field_select_column_names_as[ $c ] ) && $field_select_column_names_as[ $c ] != "" ) {  // if column name needs to be set as alias in query
				$select .= " AS `" . $field_select_column_names_as[ $c ]."`";                                   // make the alias of column name to have `` back ticks
				$column.="_as_".$field_select_column_names_as[ $c ];                                            // make $column variable to be used in the bind parameters in mysql query
				if ( isset( $field_select_column_names[ $c + 1 ] ) ) {                                          // if isset the next value in the select columns array
					$select .= ", <br>";                                                                        // then append , (comma) and a line break
				}
			} else {
				if ( isset( $field_select_column_names[ $c + 1 ] ) ) {                                          // if columns alias is not set then use the column name directly
					$select .= ", <br>";                                                                        // then directly append , and line break, if next column name is set in array
				}
			}
			$bindParam[]="$".$column;                                                                           // add column name as $ append in start, for adding bind parameter in query
			$bindS.="s";                                                                                        // binding sss for the column in the query
		}

		$query       = 'SELECT <br>' . $select . " <br>FROM `" . $db_table_name1 . "` ";                        // start writing query, where $select is column names, and $db_table_name1 is table name 1
		if ( isset( $field_as_table1 ) ) {                                                                      // If table name is having AS alias set
			$query .= "AS `" . $field_as_table1 . "` <br>";                                                     // then append AS and alias name to query string
		}
		$tablesCount = count( $db_table_name2 );                                                                // count number of tables added in "on table 2"
		if ( $tablesCount > 0 ) {                                                                                                                           // if join on table 2 array is not empty
			for ( $i = 0; $i < $tablesCount; $i ++ ) {                                                                                                      // loop through all joining tables
				$query .= $field_join_type[ $i ] . " `" . $db_table_name2[ $i ] . "` AS `" . $field_as_table2[ $i ] . "` ON `" .                            // make join query
				str_replace(".","`.`",$field_table_on_1[ $i ]) . "` " . $field_table_condition[ $i ] . " `" .
				str_replace(".","`.`",$field_table_on2[ $i ]) . "` <br>";
			}
		}
	}
	$where="";                                                                                                                                              // declare variable for storing where condition
	if ( count( $field_where_columns ) > 0 && $field_where_columns != "" ) {                                                                                // if where condition columns array is not empty
		$where = "WHERE ";                                                                                                                                  // start WHERE condition
		for ( $w = 0; $w < count( $field_where_columns ); $w ++ ) {                                                                                         // loop through all the where conditions array
			if ( isset( $field_where_columns[ $w ] ) && $field_where_columns[ $w ] != "" && $field_where_columns[ $w ] != "select" ) {                      // check if where column name is not empty or its not the default value select
				if(isset($field_where_columns_compare_values[$w]) && $field_where_columns_compare_values[$w] != "") {                                       // check for the column name of where condition, to which the value needs to compare
					$where .= "`" . str_replace( ".", "`.`", $field_where_columns[ $w ] ) . "` " . $field_where_columns_conditions[ $w ];    // put back ticks for column names of where condition
					if($field_where_columns_compare_values[$w] == "?") {                                                                                    // check if value to compare with is ?
						$where .= " " . $field_where_columns_compare_values[ $w ];                                                                          // then append it in where condition string
					}
					else{
						$where .= " '" . str_replace( ".", "`.`", $field_where_columns_compare_values[ $w ] ) . "'";                         // replace . with `.` in where condition for applying back ticks
					}
				}
				elseif(isset($field_where_columns_compare_columns[$w]) && $field_where_columns_compare_columns[$w] != "") {                                 // or if where condition column 1 needs to compare with another column
					$where .= "`" . str_replace( ".", "`.`", $field_where_columns[ $w ] ) . "` " . $field_where_columns_conditions[ $w ];    // replace . with `.` and apply back ticks in where condition
					$where.= " `" . str_replace( ".", "`.`", $field_where_columns_compare_columns[ $w ] ) . "`";                             // replace . with `.` and apply back ticks in where condition
				}
			}
			if ( isset( $field_where_columns[ $w + 1 ] ) && $field_where_columns[ $w + 1 ] != "" && $field_where_columns[ $w + 1 ] != "select" ) {          // if next where condition column name is set,
				$where .= " AND ";                                                                                                                          // then put AND in where condition and loop again
			}
		}
	}
	$groupBy = "";                                                                                                                                          // declare group by string variable
	if ( $field_group_by_columns_selected != "" ) {                                                                                                         // if group by columns array is not empty
		$gcount = count( $field_group_by_columns_selected );                                                                                                // count columns in group by array
		if ( $gcount > 0 ) {                                                                                                                                // if group by columns count is > 0
			$groupBy = ' <br>GROUP BY ';                                                                                                                    // start group by query
			for($g=0;$g<$gcount;$g++) {                                                                                                                     // loop through array of group by column names
				$groupBy .=  "`".str_replace( ".", "`.`",$field_group_by_columns_selected[$g])."`";                                          // replace . with `.` for putting back ticks in group by string
				if(isset($field_group_by_columns_selected[$g+1])){                                                                                          // if next column name is set in group by array
					$groupBy.=", ";                                                                                                                         // append , for loop through the next columns of group by
				}
			}
		}
	}
	$orderBy = "";                                                                                                                                          // declare order by string variable
	if ( $field_order_by_columns_selected != "" ) {                                                                                                         // if order by columns array is not empty
		$oCount = count( $field_order_by_columns_selected );                                                                                                // count columns in order by array
		if ( $oCount > 0 ) {                                                                                                                                // if order by count is > 0
			$orderBy = ' <br>ORDER BY ';                                                                                                                    // start writing order by string to be append in query
			for($o=0;$o<$oCount;$o++) {                                                                                                                     // loop through all order by columns
				$orderBy .=  "`".str_replace( ".", "`.`",$field_order_by_columns_selected[$o])."`";                                          // replace . with `.` for putting back tickes to column names in order by string
				if(isset($field_order_by_columns_selected[$o+1])){                                                                                          // if isset next column in order by columns array
					$orderBy .=", ";                                                                                                                        // then put , for appending next order by column
				}
			}
		}
	}
	$limit = "";                                                                                                                                            // declare a variable to write string of limit in query
	if ( $field_limit_min != "" ) {                                                                                                                         // if min limit variable is not blank
		$limit = " <br>LIMIT " . $field_limit_min;                                                                                                          // then write its value inside LIMIT variable
		if ( $field_limit_max != "" ) {                                                                                                                     // if max limit variable is not blank
			$limit .= ", " . $field_limit_max;                                                                                                              // then put it in the LIMIT string
		}
	}
	$q=$query . $where . $groupBy . $orderBy . $limit;                                                                                                      // append all the variables in 1 sql query, like query, where, groupBy, orderBy and limit
	if(isset($q) && $q != "") {
		// if sql query is not empty
		// start writing final query string variable to display in output, and append all the variables like bind parameters and bind ssss and the query name
		$queryString = '$query_' . $query_name . '= "' . $q . '";

		$stmt_query_name = $connection->prepare($' . $query_name . ');
		$stmt_query_name->bind_param("'.$bindS.'",'.implode(",",$bindParam).');

		if(!$stmt_query_name->execute()){
			echo $stmt_query_name->error;
			$stmt_query_name->free_result();
			$stmt_query_name->close();
			mysqli_close($connection);
			exit(\'failed - ' . $query_name . '\');
		}else {
			$stmt_query_name->store_result(); //Transfers a result set from a prepared statement
			$total_results = $stmt_query_name->num_rows;

			if ($total_results > 0 ) {
				/* Get the result */
				$result = $stmt_query_name->get_result();

				/* Get the number of rows */
				$num_of_rows = $stmt_query_name->num_rows;


				while ($row = $stmt_query_name->fetch_assoc()) {
					foreach($row as $cname => $cvalue){
						print "$cname: $cvalue\t";
					}
					print "\r\n";
				}




			}else {
				echo \'no results header 1\';
			}
		}';

		echo $queryString; // display $queryString variable
	}

	$query_update_original    = "DELETE FROM `builder6_canvas_query` WHERE `query_id`=?";

	if ($stmt_update_original = $connection->prepare($query_update_original)) {
		$stmt_update_original->bind_param('i',$query_id);

		if(!$stmt_update_original->execute()){
			echo $stmt_update_original->error;
			$stmt_update_original->free_result();
			$stmt_update_original->close();
			exit();
		}else {
			$new_query_id = $stmt_update_original->insert_id;
		}
	}else {
		echo 'prepare failed - insert query 1';
	}
	if ($query_id > 0) {
		$query_save_query_db    = "INSERT INTO `builder6_canvas_query`(`query_id`, `query_type`, `query_name`, `full_query`, `query_parts_prefill`,`project_id`) VALUES (?,?,?,?,?,?)";
	}else {
		$query_save_query_db    = "INSERT INTO `builder6_canvas_query`(`query_id`, `query_type`, `query_name`, `full_query`, `query_parts_prefill`,`project_id`) VALUES (default,?,?,?,?,?)";
	}
	if ($stmt_save_query_db = $connection->prepare($query_save_query_db)) {
		if ($query_id > 0) {
			$stmt_save_query_db->bind_param('issssi',$query_id,$query_type,$query_name,$full_query,$query_parts_prefill,$project_id);
		}else {
			$stmt_save_query_db->bind_param('ssssi',$query_type,$query_name,$full_query,$query_parts_prefill,$project_id);
		}

		$query_type	 = 'select';
		$query_name	 = 'query_' . $query_array[0]['query_name'];
		$full_query	 = $queryString;
		$query_parts_prefill = json_encode($query_array);
		$page_id	 	 = $_SESSION['page_id'];
		$section_id	 = $_SESSION['section_id'];
		$project_id	 = $_SESSION['project_id'];

		if(!$stmt_save_query_db->execute()){
			echo $stmt_save_query_db->error;
			$stmt_save_query_db->free_result();
			$stmt_save_query_db->close();
			exit();
		}else {
			$new_query_id = $stmt_save_query_db->insert_id;
		}
	}else {
		echo 'prepare failed - insert query 2';
	}

	/*
	foreach ($array_query_page_list as $value) {
		$query_insert_query_page_list    = "";

		if ($stmt_insert_query_page_list = $connection->prepare($query_insert_query_page_list)) {
			$stmt_insert_query_page_list->bind_param('i',$query_id, $value);

			if(!$stmt_insert_query_page_list->execute()){
				echo $stmt_insert_query_page_list->error;
				$stmt_insert_query_page_list->free_result();
				$stmt_insert_query_page_list->close();
				exit();
			}else {
				$new_query_id = $stmt_insert_query_page_list->insert_id;
			}
		}else {
			echo 'prepare failed - insert query 1';
		}
	}
	*/






	// create directory
	if(!file_exists($_SERVER['DOCUMENT_ROOT'] . '/projects' . '/' . $project_directory . '/' . $section_name . '/queries')){
		mkdir($_SERVER['DOCUMENT_ROOT'] . '/projects' . '/' . $project_directory . '/' . $section_name . '/queries',0777);
	}
	// delete old file
	if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/projects' . '/' . $project_directory . '/' . $section_name . '/queries' . '/' . $query_name . '.php')) {
		unlink($_SERVER['DOCUMENT_ROOT'] . '/projects' . '/' . $project_directory . '/' . $section_name . '/queries' . '/' . $query_name . '.php');
	}
	// create new file
	$query_template_file   = $_SERVER['DOCUMENT_ROOT'] . '/projects' . '/' . $project_directory . '/' . $section_name . '/queries' . '/' . $query_name . '.php';
	$query_file = fopen($query_template_file, "w");
	fwrite($query_file, $queryString);
	fclose($query_file);

	$_SESSION['query_id'] = $new_query_id;


}
?>
