<div class="panel">
          <div class="panel-heading" role="tab">
                    <a class="panel-title collapsed" data-parent="#accordion_build_query" data-toggle="collapse" href="#tab_add_database" aria-controls="tab_add_database" aria-expanded="true">
                              Add Database
                    </a>
          </div>
          <div class="panel-collapse collapse" id="tab_add_database" role="tabpanel" style="">
                    <div class="panel-body">
                              <div class="row">
                                        <div class="form-group col-12">
                                                  <select id="field_database_name" name="field_database_name" class="form-control">
                                                            <option value="select">Database Name</option>
                                                            <?php foreach ($databases as $value): ?>
                                                                      <option value="<?php echo $value ?>"><?php echo $value ?></option>
                                                            <?php endforeach; ?>
                                                  </select>
                                        </div>
                              </div>
                    </div>
          </div>
</div>
