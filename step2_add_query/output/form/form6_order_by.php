<div class="panel">

          <div class="panel-heading" role="tab">
                    <a class="panel-title collapsed" data-parent="#accordion_build_query" data-toggle="collapse" href="#tab_add_order_by" aria-controls="tab_add_order_by" aria-expanded="false">
                              Add Order By
                    </a>
          </div>
          <div class="panel-collapse collapse" id="tab_add_order_by" role="tabpanel">
                    <div class="panel-body">
                              <div class="row">
                                        <div class="col-md-12 duplicate_append_to_inner">
                                                  <div class="row ">
                                                            <div class="col-6 form-group">
                                                                                <select name="field_order_by_columns_list[]" class="h-150 form-control move_left fill_with_all_table_columns" multiple="multiple">
                                                                                          <option value="" selected disabled>Column Name</option>
                                                                                </select>

                                                            </div>

                                                            <div class="col-6 form-group">
                                                                                <select name="field_order_by_columns_selected[]" class="h-150 form-control move_right" multiple="multiple">

                                                                                </select>

                                                            </div>
                                                  </div>
                                        </div>
                              </div>
                    </div>
          </div>
</div>
