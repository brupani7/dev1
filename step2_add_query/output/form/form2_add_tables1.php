<div class="panel">
          <center>
                    <div class="panel-heading" role="tab">
                              <a class="panel-title collapsed" data-parent="#accordion_build_query" data-toggle="collapse" href="#tab_add_tables" aria-controls="tab_add_tables" aria-expanded="false">
                                        Add Tables
                              </a>
                    </div>
          </center>
          <div class="panel-collapse collapse" id="tab_add_tables" role="tabpanel" style="">
                    <div class="panel-body duplicate_append_to">
                              <div class="row">
                                        
                                        <div id="table_row_fields" class="col-md-8 duplicate_append_to_inner">

                                                  <div class="row row_master table_row">
                                                            <a href="#" class="table_row_delete">Delete Row</a>
                                                            <div class="form-group col-md-10">
                                                                      <select name="db_table_name1[]" class="form-control table_name table_name1" onchange="tableColumns1(this,this.value)">
                                                                                <option value="" selected disabled>Table</option>
                                                                      </select>
                                                            </div>
                                                            <div class="form-group col-md-10">
                                                                      <input type="text" name="field_as_table1[]" class="form-control table_name_as1" placeholder="AS" value="" onchange="tableColumnsAs1(this,this.value)">
                                                            </div>
                                                            <div class="form-group col-md-10">
                                                                      <select name="field_join_type[]" class="form-control">
                                                                                <option value="" disabled selected>Join Type</option>
                                                                                <option value="INNER JOIN">Inner Type</option>
                                                                                <option value="LEFT JOIN">Left Type</option>
                                                                                <option value="RIGHT JOIN">Right Type</option>
                                                                      </select>
                                                            </div>
                                                            <div class="form-group col-md-10">
                                                                      <select name="db_table_name2[]" class="form-control table_name table_name2" onchange="tableColumns2(this,this.value)">
                                                                                <option value="select">Table</option>
                                                                      </select>
                                                            </div>
                                                            <div class="form-group col-md-10">
                                                                      <input type="text" name="field_as_table2[]" class="form-control table_name_as2" placeholder="AS" value="" onchange="tableColumnsAs2(this,this.value)">
                                                            </div>
                                                            <div class="form-group col-md-10">
                                                                      <select name="field_table_on_1[]" class="form-control  fill_with_all_table_columns1">
                                                                                <option value="select">ON 1</option>
                                                                      </select>
                                                            </div>
                                                            <div class="form-group col-md-10">
                                                                      <select name="field_table_condition[]" class="form-control">
                                                                                <option value="" selected disabled>CONDITION</option>
                                                                                <option value="=">  =  </option>
                                                                                <option value="!="> != </option>
                                                                                <option value="<">  <  </option>
                                                                                <option value=">">  >  </option>
                                                                                <option value=">="> >= </option>
                                                                                <option value="<="> <= </option>
                                                                      </select>
                                                            </div>
                                                            <div class="form-group col-md-10">
                                                                      <select name="field_table_on2[]" class="form-control fill_with_all_table_columns2">
                                                                                <option value="select">ON 2</option>
                                                                      </select>
                                                            </div>
                                                  </div>
                                                  <div class="row">
                                                            <div class="col-12">
                                                                      <button type="button" class="duplicate_row btn btn-primary waves-effect waves-classic">Add Another Table</button>
                                                            </div>
                                                  </div>
                                        </div>
                              </div>
                    </div>
          </div>
</div>
