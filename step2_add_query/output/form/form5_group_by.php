<div class="panel">

          <div class="panel-heading" role="tab">
                    <a class="panel-title collapsed" data-parent="#accordion_build_query" data-toggle="collapse" href="#tab_add_group_by" aria-controls="tab_add_group_by" aria-expanded="false">
                              Add Group By
                    </a>
          </div>
          <div class="panel-collapse collapse" id="tab_add_group_by" role="tabpanel">
                    <div class="panel-body">
                              <div class="row">
                                        <div  class="col-md-12 duplicate_append_to_inner">
                                                  <div class="row">
                                                            <div class="form-group col-6">
                                                                      <select name="field_group_by_columns_list[]" class="h-150 form-control move_left fill_with_all_table_columns" multiple="multiple">
                                                                                <option value="">Column Name</option>
                                                                      </select>
                                                            </div>
                                                            <div class="form-group col-6">
                                                                      <select name="field_group_by_columns_selected[]" class="h-150 form-control move_right" multiple="multiple">
                                                                      </select>
                                                            </div>
                                                  </div>
                                        </div>
                              </div>
                    </div>
          </div>
</div>
