<div class="panel">
          <div class="panel-heading" role="tab">
                    <a class="panel-title collapsed" data-parent="#accordion_build_query" data-toggle="collapse" href="#tab_add_fields" aria-controls="tab_add_fields" aria-expanded="false">
                              Select Columns
                    </a>
          </div>
          <div class="panel-collapse collapse" id="tab_add_fields" role="tabpanel">
                    <div class="panel-body duplicate_append_to">
                              <div class="row">
                                        <div class="col-md-12 duplicate_append_to_inner">
                                                  <div class="row table_row">
                                                            <div class="form-group col-6">
                                                                      <div class="list-group">
                                                                                <select name="field_select_column_names[]" class="fill_with_all_table_columns form-control">
                                                                                          <a class="list-group-item " href="#">Column Name</a>
                                                                                </select>
                                                                      </div>
                                                                      <a href="#" class="table_row_delete">Delete Column</a>
                                                            </div>
                                                            <div class="form-group col-6">
                                                                      <input type="text" name="field_select_column_names_as[]" class="form-control" placeholder="AS" value="">
                                                            </div>
                                                  </div>
                                        </div>
                              </div>
                              <div class="row">
                                        <div class="col-12">
                                                  <button type="button" class="duplicate_row btn btn-primary waves-effect waves-classic">Add Column</button>
                                        </div>
                              </div>
                    </div>
          </div>
</div>
